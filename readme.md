LINUX:

1.  Install node.js and npm:

1.1 Download and import the Nodesource GPG key:
    sudo apt-get update
    sudo apt-get install -y ca-certificates curl gnupg
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

1.2 Create deb repository:
    NODE_MAJOR=18
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list

1.3 Run Update and Install:
    sudo apt-get update
    sudo apt-get install nodejs -y

2.  Install typescript:
    npm install -g typescript

3.  Instal dependencies:
    npm install

4.  Translate typescript into javascript:
    tsc

5.  Run javascript
    node amogus.js

WINDOWS

1. Install node.js and npm from https://nodejs.org/en/download

2. Install typescript: npm install -g typescript

3. Install dependencies: npm install

4. Translate typescript into javascript:
   npx tsc

5. Run javascript
   node amogus.js